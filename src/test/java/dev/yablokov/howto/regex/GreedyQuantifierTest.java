package dev.yablokov.howto.regex;

import org.testng.annotations.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.testng.Assert.*;

/**
 * Тестирование работы Greedy Quantifier'а регулряных выражений
 * ToDo: описать особенность работы данного Quantifier'а
 *
 * @author Яблоков Максим Вадимович {@literal <maxim@yablokov.dev>}
 */
@SuppressWarnings({"squid:S1192", "squid:S00100"})
public class GreedyQuantifierTest {
    @Test
    public void matches_ByPatternAndMatcher() {
        Pattern pattern = Pattern.compile("\\d*0");
        Matcher matcher = pattern.matcher("1234567890");
        assertTrue(matcher.matches());

        pattern = Pattern.compile("^\\d*0$");
        matcher = pattern.matcher("1234567890");
        assertTrue(matcher.matches());

        pattern = Pattern.compile("^\\d*1$");
        matcher = pattern.matcher("1234567890");
        assertFalse(matcher.matches());
    }

    @Test
    public void matches_ByEmbeddedInStringMethod() {
        assertTrue("1234567890".matches("\\d*0"));
        assertTrue("1234567890".matches("^\\d*0$"));
        assertFalse("1234567890".matches("^\\d*1$"));
    }

    @Test
    public void matches_ByEmbeddedInStringMethod_UsingUnnamedCapturingGroups() {
        assertTrue("abc123 abc123".matches("(\\w+) (\\1)"));
        assertFalse("abc123 cba321".matches("(\\w+) (\\1)"));
    }

    @Test
    public void matches_ByEmbeddedInStringMethod_UsingNamedCapturingGroups() {
        assertTrue("abc123 abc123".matches("(?<firstWord>\\w+) (\\k<firstWord>)"));
        assertFalse("abc123 cba321".matches("(?<firstWord>\\w+) (\\k<firstWord>)"));
    }

    @Test
    public void replaceAll_ByEmbeddedInStringMethod() {
        String replaced = "!Original Original Original!".replaceAll("\\w+", "Replaced");
        assertEquals(replaced, "!Replaced Replaced Replaced!");

        replaced = "!Original Original Original!".replaceAll("^\\w+$", "Replaced");
        assertEquals(replaced, "!Original Original Original!");
    }

    @Test
    public void replaceAll_ByEmbeddedInStringMethod_UsingCapturingGroups() {
        String replaced = "!FirstWord SecondWord!".replaceAll("(\\w+) (\\w+)", "$2 $1");
        assertEquals(replaced, "!SecondWord FirstWord!");

        replaced = "!FirstWord SecondWord!".replaceAll("(?<firstGroup>\\w+) (?<secondGroup>\\w+)", "${secondGroup} ${firstGroup}");
        assertEquals(replaced, "!SecondWord FirstWord!");
    }

    @Test
    public void find_InEntireInput_PositiveCase() {
        Pattern pattern = Pattern.compile("^(\\w+)(\\w+)$");
        Matcher matcher = pattern.matcher("abc123");

        assertTrue(matcher.find());

        assertEquals(matcher.group(1), "abc12");
        assertEquals(matcher.start(1), 0);
        assertEquals(matcher.end(1), 5);

        assertEquals(matcher.group(2), "3");
        assertEquals(matcher.start(2), 5);
        assertEquals(matcher.end(2), 6);
    }

    @Test
    public void find_InEntireInput_NegativeCase() {
        Pattern pattern = Pattern.compile("^(\\w+)(\\w+)$");
        Matcher matcher = pattern.matcher("!abc123!");
        assertFalse(matcher.find());
    }

    @Test
    public void find_InPartOfInput_PositiveCase() {
        Pattern pattern = Pattern.compile("(\\w+)(\\w+)");
        Matcher matcher = pattern.matcher("!abc123!");

        assertTrue(matcher.find());

        assertEquals(matcher.group(1), "abc12");
        assertEquals(matcher.start(1), 1);
        assertEquals(matcher.end(1), 6);

        assertEquals(matcher.group(2), "3");
        assertEquals(matcher.start(2), 6);
        assertEquals(matcher.end(2), 7);

        assertFalse(matcher.find());
    }

    @Test
    public void find_UsingPositiveLookahead() {
        Pattern pattern = Pattern.compile("(\\d+)(?=78)");
        Matcher matcher = pattern.matcher("!1234567890!");

        assertTrue(matcher.find());

        assertEquals(matcher.group(1), "123456");
        assertEquals(matcher.start(1), 1);
        assertEquals(matcher.end(1), 7);
    }

    @Test
    public void find_UsingNegativeLookahead() {
        Pattern pattern = Pattern.compile("(\\d+)(?!\\w)");
        Matcher matcher = pattern.matcher("!12345abc67890!");

        assertTrue(matcher.find());

        assertEquals(matcher.group(1), "67890");
        assertEquals(matcher.start(1), 9);
        assertEquals(matcher.end(1), 14);
    }

    @Test
    public void find_UsingPositiveLookbehind() {
        Pattern pattern = Pattern.compile("(?<=34)(\\d+)");
        Matcher matcher = pattern.matcher("!1234567890!");

        assertTrue(matcher.find());

        assertEquals(matcher.group(1), "567890");
        assertEquals(matcher.start(1), 5);
        assertEquals(matcher.end(1), 11);
    }

    @Test
    public void find_UsingNegativeLookbehind() {
        Pattern pattern = Pattern.compile("(?<!\\w)(\\d+)");
        Matcher matcher = pattern.matcher("!abc123-456!");

        assertTrue(matcher.find());

        assertEquals(matcher.group(1), "456");
        assertEquals(matcher.start(1), 8);
        assertEquals(matcher.end(1), 11);
    }
}
