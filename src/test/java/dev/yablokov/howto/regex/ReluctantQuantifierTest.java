package dev.yablokov.howto.regex;

import org.testng.annotations.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.testng.Assert.*;

/**
 * Тестирование работы Reluctant Quantifier'а регулряных выражений
 * ToDo: описать особенность работы данного Quantifier'а
 *
 * @author Яблоков Максим Вадимович {@literal <maxim@yablokov.dev>}
 */
@SuppressWarnings({"squid:S1192", "squid:S00100"})
public class ReluctantQuantifierTest {
    @Test
    public void matches_ByPatternAndMatcher() {
        Pattern pattern = Pattern.compile("\\d*?0");
        Matcher matcher = pattern.matcher("1234567890");
        assertTrue(matcher.matches());

        pattern = Pattern.compile("^\\d*?0$");
        matcher = pattern.matcher("1234567890");
        assertTrue(matcher.matches());

        pattern = Pattern.compile("^\\d*?1$");
        matcher = pattern.matcher("1234567890");
        assertFalse(matcher.matches());
    }

    @Test
    public void matches_ByEmbeddedInStringMethod() {
        assertTrue("1234567890".matches("\\d*?0"));
        assertTrue("1234567890".matches("^\\d*?0$"));
        assertFalse("1234567890".matches("^\\d*?1$"));
    }

    @Test
    public void matches_ByEmbeddedInStringMethod_UsingUnnamedCapturingGroups() {
        assertTrue("abc123 abc123".matches("(\\w+?) (\\1)"));
        assertFalse("abc123 cba321".matches("(\\w+?) (\\1)"));
    }

    @Test
    public void matches_ByEmbeddedInStringMethod_UsingNamedCapturingGroups() {
        assertTrue("abc123 abc123".matches("(?<firstWord>\\w+?) (\\k<firstWord>)"));
        assertFalse("abc123 cba321".matches("(?<firstWord>\\w+?) (\\k<firstWord>)"));
    }

    @Test
    public void replaceAll_ByEmbeddedInStringMethod() {
        String replaced = "!123!".replaceAll("\\w+?", "Replaced");
        assertEquals(replaced, "!ReplacedReplacedReplaced!");

        replaced = "!Original!".replaceAll("^\\w+?$", "Replaced");
        assertEquals(replaced, "!Original!");
    }

    @Test
    public void replaceAll_ByEmbeddedInStringMethod_UsingCapturingGroups() {
        String replaced = "!123 456!".replaceAll("(\\w+?) (\\w+?)", "$2 $1");
        assertEquals(replaced, "!4 12356!");

        replaced = "!123 456!".replaceAll("(?<firstGroup>\\w+?) (?<secondGroup>\\w+?)", "${secondGroup} ${firstGroup}");
        assertEquals(replaced, "!4 12356!");
    }

    @Test
    public void find_InEntireInput_PositiveCase() {
        Pattern pattern = Pattern.compile("^(\\w+?)(\\w+?)$");
        Matcher matcher = pattern.matcher("abc123");

        assertTrue(matcher.find());

        assertEquals(matcher.group(1), "a");
        assertEquals(matcher.start(1), 0);
        assertEquals(matcher.end(1), 1);

        assertEquals(matcher.group(2), "bc123");
        assertEquals(matcher.start(2), 1);
        assertEquals(matcher.end(2), 6);
    }

    @Test
    public void find_InEntireInput_NegativeCase() {
        Pattern pattern = Pattern.compile("^(\\w+?)(\\w+?)$");
        Matcher matcher = pattern.matcher("!abc123!");
        assertFalse(matcher.find());
    }

    @Test
    @SuppressWarnings("squid:S1199")
    public void find_InPartOfInput_PositiveCase() {
        Pattern pattern = Pattern.compile("(\\w+?)(\\w+?)");
        Matcher matcher = pattern.matcher("!1234!");
        {
            assertTrue(matcher.find());

            assertEquals(matcher.group(1), "1");
            assertEquals(matcher.start(1), 1);
            assertEquals(matcher.end(1), 2);

            assertEquals(matcher.group(2), "2");
            assertEquals(matcher.start(2), 2);
            assertEquals(matcher.end(2), 3);
        }
        {
            assertTrue(matcher.find());

            assertEquals(matcher.group(1), "3");
            assertEquals(matcher.start(1), 3);
            assertEquals(matcher.end(1), 4);

            assertEquals(matcher.group(2), "4");
            assertEquals(matcher.start(2), 4);
            assertEquals(matcher.end(2), 5);
        }
    }
}
